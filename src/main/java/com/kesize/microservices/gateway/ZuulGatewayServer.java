package com.kesize.microservices.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/*
 * Example from :
 * https://medium.com/@migueldoctor/spring-cloud-series-crea-y-configura-paso-a-paso-un-servicio-gateway-para-routing-y-balanceo-de-aaf79398d39e
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulGatewayServer {
	
	public static void main(String[] args) {
        SpringApplication.run(ZuulGatewayServer.class, args);
    }
}
